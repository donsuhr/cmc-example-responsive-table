﻿/**
 * @fileOverview This file renders the UI
 * @name App.UI
 */
//

App.UI = {};

//Self invoking function
(function (appUi, service) {

    Handlebars.registerHelper('rowId', function (value) {
        return value || '-1';
    });
    Handlebars.registerHelper('selected', function (value1, value2) {
        return value1 === value2 ? 'selected' : '';
    });
    Handlebars.registerHelper('checked', function (value1, value2) {
        return value1 === value2 ? 'checked' : '';
    });
    $.fn.filterByData = function (prop, val) {
        return this.filter(
            function () {
                return $(this).data(prop) == val;
            }
        );
    };

    $.fn.debounce = function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) {
                    func.apply(context, args);
                }
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) {
                func.apply(context, args);
            }
        };
    };

    /**
     * This is the starting point, this method is invoked on page load (i.e. On document.ready)
     */
    appUi.LoadContactPreferences = function () {
        /** Build table rows and append to the table */
        $("#PreferencesTable").html(appUi.BuildRows);
        // adding the events to the page.
        addListeners();
        setTimeout(appUi.setOrigHeight, 500);
    };

    /**
     * Dynamically builds the table rows with the required categories and groups data
     * @returns {string}  Html table rows
     */
    appUi.BuildRows = function () {
        var listOfPreferences = service.FetchAllContactPreferencesForContact();
        if (listOfPreferences == null) {
            return '<p class="error">There are no preferences found for the contact.</p>';
        }
        var source = document.getElementById('manage-preferences-template').innerHTML;
        var template = Handlebars.compile(source);
        return template(listOfPreferences);
    };

    appUi.setOrigHeight = function () {
        if ($('html').hasClass('lt-ie10')){
            // ie 9 does not respect tbody display block or height, abort
            return;
        }
        $('.manage-preferences__table tbody').each(function (index, element) {
            var $tbody = $(element);
            var currentHeight = parseInt($tbody.css('height'));
            $tbody.css('height', '');
            //
            var forceRedraw = element.outerHeight;
            //
            var maxHeight = $tbody.outerHeight(true);
            $tbody.data('origheight', maxHeight);
            var currentToggle = $tbody.hasClass('is-closed');
            $tbody.css('height', currentHeight);
            appUi.toggleCategory($tbody, currentToggle);
        });
    };

    appUi.toggleCategory = function toggleCategory($tbody, forceClose) {
        var rowHeight = $tbody.find('tr:first').outerHeight(true),
            origHeight = $tbody.data('origheight');
        var currentToggle = $tbody.hasClass('is-closed');
        var targetHeight = currentToggle ? origHeight : rowHeight;
        if (typeof forceClose !== 'undefined') {
            targetHeight = forceClose ? rowHeight : origHeight;
        }
        $tbody.toggleClass('is-closed', forceClose);
        $tbody.animate({
            height: targetHeight
        });
        if (targetHeight === origHeight) {
            $tbody.find('input').attr('tabindex', '');
        } else {
            $tbody.find('input').attr('tabindex', '-1');
        }
    };

    appUi.setHeaderHeight = function toggleCategory(event) {
        var isScrolled = $(document).scrollTop() > 15;
        $('.manage-preferences-page__header-wrapper').toggleClass('manage-preferences-page__header-wrapper--scrolled', isScrolled);
    };

    var addListeners = function () {
        $(window).on('resize', $.fn.debounce(appUi.setOrigHeight, 1000));
        $(window).on('scroll', $.fn.debounce(appUi.setHeaderHeight, 250));

        $('.manage-preferences__table button').on('click', function (event) {
            event.preventDefault();
            var $tbody = $(event.target).closest('tbody');
            appUi.toggleCategory($tbody);
        });

        $('.manage-preferences__expand-all-button').on('click', function (event) {
            event.preventDefault();
            $('.manage-preferences__table tbody').each(function (index, element) {
                appUi.toggleCategory($(element), false);
            });
        });

        $('.manage-preferences__collapse-all-button').on('click', function (event) {
            event.preventDefault();
            $('.manage-preferences__table tbody').each(function (index, element) {
                appUi.toggleCategory($(element), true);
            });
        });

        // on change sets the dirty flag on Email/Sms/Letter dropdown controls
        $('.manage-preferences__table :checkbox').on('change', function (event) {
            var $this = $(this);
            $this.closest('td').data('isDirty', true); // cell dirty
            $this.closest('tr').data('isDirty', true); // row dirty
        });

        $('#updatePreferences').click(function () {
            var mediaSetting = {
                None: 1,
                Email: 2,
                Sms: 4,
                Letter: 8
            };

            var request = {
                "preferences": [],
                "preferenceRequestDetails": {
                    "objectType": App.ObjectType,
                    "objectId": App.ObjectId,
                    "token": App.TokenString
                }
            };

            var rowIdCounter = 0,
                $row,
                emailInput,
                isEmailDirty,
                smsInput,
                isSmsDirty,
                letterInput,
                isLetterDirty,
                rowId,
                preferenceInfo;

            $('.manage-preferences__table tr').filterByData('isDirty', true).each(function (i, row) {
                $row = $(row);
                emailInput = $row.children().filterByData('pid', 'EMAIL').find('input');
                isEmailDirty = emailInput.closest('td').data('isDirty');
                smsInput = $row.children().filterByData('pid', 'SMS').find('input');
                isSmsDirty = smsInput.closest('td').data('isDirty');
                letterInput = $row.children().filterByData('pid', 'LETTER').find('input');
                isLetterDirty = letterInput.closest('td').data('isDirty');

                rowId = parseInt($row.data('rowid'));

                preferenceInfo = {
                    "id": rowId > 0 ? rowId : --rowIdCounter,
                    "categoryId": $row.data('categoryid'),
                    "groupId": $row.data('groupid'),
                    "email": isEmailDirty ? emailInput.is(':checked') : null,
                    "sms": isSmsDirty ? smsInput.is(':checked') : null,
                    "letter": isLetterDirty ? letterInput.is(':checked') : null,
                    "preferencesModified": (isEmailDirty ? mediaSetting.Email : 0) |
                    (isSmsDirty ? mediaSetting.Sms : 0) |
                    (isLetterDirty ? mediaSetting.Letter : 0)
                };
                request.preferences.push(preferenceInfo);
            });

            service.UpdateContactPreferencesForContact(request, function (data) {
                if (data != null && data.validationMessagesField && data.validationMessagesField.length > 0) {
                    alert(data.validationMessagesField[0].messageTextField);
                }
            });

        });
    };
})(App.UI, App.Service);

$(document).ready(
    function init() {
        App.UI.LoadContactPreferences();
    }
);
