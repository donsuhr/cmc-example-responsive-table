// Generated on 2014-10-23 using
// generator-webapp 0.5.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
    require('time-grunt')(grunt);
    // Configurable paths
    var config = {
        app: 'app',
        dist: 'dist',
        sassOutput: '.tmp/sass-out/'
    };

    // Time how long tasks take. Can help when optimizing build times

    require('load-grunt-config')(grunt, {
        configPath: require('path').join(process.cwd(), '/build/grunt'),//'./build/grunt',
        data: config,
        jitGrunt: {
            staticMappings: {
                scsslint: 'grunt-scss-lint',
                replace: 'grunt-text-replace',
                browserSync: 'grunt-browser-sync',
            }
        }
    });

    grunt.registerTask('serve', function () {
        grunt.task.run([
            'clean',
            'sass',
            'autoprefixer',
            'concurrent:serveSupport'
        ]);
    });

    grunt.registerTask('dist', ['build', 'notify:dist']);

};
