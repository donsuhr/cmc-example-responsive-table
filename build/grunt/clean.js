module.exports = function (grunt, options) {
    'use strict';
    return {
        dev: {
            files: [
                {
                    dot: true,
                    src: [
                        '.tmp'
                    ]
                }
            ]
        },
        dist: {
            files: [
                {
                    dot: true,
                    src: [
                        '.require-build',
                        options.dist + '/*',
                        '!' + options.dist + '/.git*'
                    ]
                }
            ]
        }
    };
};
