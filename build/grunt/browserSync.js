module.exports = function (grunt, options) {
    'use strict';
    return {
        bsFiles: {
            src: ['.tmp/styles/*.css', 'app/*.html', '.tmp/images/*.*', '.tmp/scripts/**/*.js']
        },
        options: {
            open: 'external',
            notify: false,
            server: {
                //browser: "google chrome",
                baseDir: [".tmp", "./app"],
                //index: "index.html",
                //directory: true,
                routes: {
                    "/node_modules": "./node_modules",
                    //"/require-build": "./.require-build",
                    "/dist": "./dist",
                    "/api": "./api-data",
                    "/orig": "./orig"
                },
                middleware: [
                    function (req, res, next) {
                        if (req.method === 'POST') {
                            req.method = 'GET';
                        }
                        return next();
                    }
                ]
            }
        }
    };
};
