module.exports = function (grunt, options) {
    'use strict';
    return {
        options: {
            logConcurrentOutput: true,
            limit: 8
        },
        dist: [
            //'compass:dist',
            'sass',
            'copy:dist'
        ],
        serveSupport: [
            'watch:sass',
            //'watch:js',
            'browserSync'
        ]

    };
};
