module.exports = function (grunt, options) {
    'use strict';
    return {
        js: {
            files: [
                options.app + '/scripts/{,*/}*.js',
                options.app + '/templates/{,*/}*.js'
            ],
            tasks: ['eslint', 'babel']
        },
        sass: {
            files: ['./app/styles/**/*.scss'],
            tasks: ['sass', 'autoprefixer', 'scsslint']
        },
        autoprefixer: {
            files: ['.tmp/sass-out/**/*.css'],
            tasks: ['newer:autoprefixer']
        },
        scssLint: {
            files: [options.app + '/styles/{,*/}*.{scss,sass}'],
            tasks: ['scsslint']
        }
    };
};
