module.exports = function (grunt, options) {
    'use strict';
    return {
        options: {
            sourceMap: true,
            plugins: ['transform-es2015-modules-amd']
        },
        babel: {
            files: [
                {
                    expand: true,
                    cwd: 'app/scripts',
                    src: ['**/*.js'],
                    dest: '.tmp/scripts',
                    ext: '.js'
                },
                {
                    expand: true,
                    cwd: 'app/templates',
                    src: ['**/*.js'],
                    dest: '.tmp/templates',
                    ext: '.js'
                }
            ]
        }

    };
};
