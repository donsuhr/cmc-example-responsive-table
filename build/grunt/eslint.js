'use strict';

module.exports = function (grunt, options) {
    return {
        lint: {
            options: {
                //rulePaths: ['conf/rules']
                //format: require('eslint-tap')
            },
            src: [options.app + '/**/*.js', '!app/scripts/vendor/**/*.*', '!app/scripts/prevVersionGlobals.js']
        }
    };
};


