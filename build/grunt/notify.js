module.exports = function (grunt, options) {
    'use strict';
    return {
        styles: {
            options: {
                title: 'Grunt Task',
                message: 'Styles Done'
            }
        },
        scripts: {
            options: {
                title: 'Grunt Task',
                message: 'Script Done'
            }
        },
        dist: {
            options: {
                title: 'Grunt Task',
                message: 'Dist Done'
            }
        }

    };
};
