'use strict';

module.exports = function (grunt, options) {
    return {
        options: {
            sourceMap: true,
            outputStyle: 'expanded'
        },
        dev: {
            files: [{
                expand: true,
                cwd: 'app/styles',
                src: ['**/*.scss'],
                dest: options.sassOutput,
                ext: '.css'
            }]
        }
    };
};
