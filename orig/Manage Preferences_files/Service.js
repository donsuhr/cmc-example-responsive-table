﻿/**
 * @fileOverview This file stands as a service data adapter - to communicate between the UI and the Web Api (intern iService).
 * @name App.Service
 */
//

App.Service = {};

(function (service) {
    /**
     * Fetches an collective list of 
     *  all the Categories and groups in the system and 
     *  all the Media preferences set by the user
     * @returns {type} collective list of user Preferences 
     */
    service.FetchAllContactPreferencesForContact = function () {
        var preferenceMetaList;
        $.ajax({
            url: "api/ContactPreferences/GetContactPreferencesMeta",
            type: "GET",
            async: false,
            success: function (responseData) {
                preferenceMetaList = responseData;
            },
            error: function (responseData) {
                if (responseData.status != undefined && responseData.status == 400 && responseData.responseJSON != undefined) {
                    console.log("Ajax call failed: " + responseData.responseJSON.Message);
                } else {
                    console.log("Ajax call failed Status Code: " + responseData.status + " Status Text: " + responseData.statusText);
                }
                preferenceMetaList = null;
            }
        });
        if (preferenceMetaList == null) {
            return null;
        }
        var contactsSetPreferences;
        var requestData = {
            "PreferenceRequestDetails": {
                "objectType": App.ObjectType,
                "objectId": App.ObjectId,
                "token": App.TokenString
            }
        }
        $.ajax({
            url: "api/ContactPreferences/GetContactPreferencesForInstance",
            type: "POST",
            async: false,
            data: requestData,
            success: function (responseData) {
                if (responseData.preferenceDetailsField == null || responseData.preferenceDetailsField.length == 0) {
                    if (responseData.validationMessagesField != null && responseData.validationMessagesField.length > 0) { //Error in fetching the User's prefernece data
                        //Checking for specfic error
                        if (responseData.validationMessagesField[0].codeField == "1073803824") {
                            console.log("The Contact has not set any preferences. " + responseData.validationMessagesField[0].messageTextField);
                        } else {
                            console.log("Unable to retrive the contact preferences. " + responseData.validationMessagesField[0].messageTextField);
                            preferenceMetaList = null; //Setting it to Null because there was an token decryption error
                            alert("Unable to retrive the contact preferences." + responseData.validationMessagesField[0].messageTextField);
                        }
                    }
                } else {
                    contactsSetPreferences = responseData;
                    mergePreferencesData(contactsSetPreferences, preferenceMetaList);
                }
            },
            error: function (responseData) {
                if (responseData.status != undefined && responseData.status == 400 && responseData.responseJSON != undefined) {
                    console.log("Ajax call failed: " + responseData.responseJSON.Message);
                } else {
                    console.log("Ajax call failed Status Code: " + responseData.status + " Status Text: " + responseData.statusText);
                }
                contactsSetPreferences = null;
            }
        });
        return preferenceMetaList;
    };
    /**
     * Merges User's configured preferences into the holistic preferences meta list
     * @param {type} contactsSetPreferences - holds only the preferences set by the user
     * @param {type} preferenceMetaList - holds all the preferences available in the system
     */
    var mergePreferencesData = function (contactsSetPreferences, preferenceMetaList) {
        $.each(preferenceMetaList.preferenceCategoriesField, function (categoryIndex, preferenceCategory) {
            $.each(preferenceCategory.preferenceGroupsField, function (groupIndex, preferenceGroup) {
                //Looping user's preferences and setting the 
                $.each(contactsSetPreferences.preferenceDetailsField, function (preferencesIndex, userPreference) {
                    if (preferenceGroup.idField == userPreference.groupIdField) {
                        preferenceGroup.RowId = userPreference.idField;
                        preferenceGroup.Email = userPreference.emailField;
                        preferenceGroup.Sms = userPreference.smsField;
                        preferenceGroup.Letter = userPreference.letterField;
                        return false;
                    }
                });
            });
        });
    };

    // ajax request to update preferences.
    /**
     * Update / Save the modified contact preferences 
     * @param {type} preferences - Holds the List of modified preferences 
     * @param {type} successCallback - call back function on success
     */
    service.UpdateContactPreferencesForContact = function (preferences, successCallback) {
        $.ajax({
            url: "api/ContactPreferences/UpdateContactPreferences/", // todo
            type: "POST",
            async: true,
            data: preferences,
            success: successCallback,
            error: function (data) {
                if (data.status != undefined && data.status == 400 && data.responseJSON != undefined) {
                    console.log("Ajax call failed: " + data.responseJSON.Message);
                } else {
                    console.log("Ajax call failed Status Code: " + data.status + " Status Text: " + data.statusText);
                }
            }
        });
    };
})(App.Service);