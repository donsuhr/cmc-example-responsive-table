﻿/**
 * @fileOverview This file renders the UI  
 * @name App.UI
 */
//

App.UI = {};

//Self invoking function
(function (appUi, service) {

    /**
     * This is the starting point, this method is invoked on page load (i.e. On document.ready)
     */
    appUi.LoadContactPreferences = function () {
        /** Build table rows and append to the table */
        $("#PreferencesTable").html(appUi.BuildRows);
        // adding the events to the page.
        initializeControlEvents();
    };
    var headerTemplate = "<thead>" +
                            "<th class='trCategory header' width='8%'>Category</th> " +
                            "<th class='header'>Group</th>" +
                            "<th class='tdEmailSmsLetter header'>Email</th>" +
                            "<th class='tdEmailSmsLetter header'>SMS</th>" +
                            "<th class='tdEmailSmsLetter header'>Letter</th>" +
                        "</tr> </thead>";
    var categoryTemplate = "<tr> <td colspan='5' class='trCategory otherFonts'>{0}</td></tr>";
    var groupTemplate = "<tr rowId='{0}' categoryId='{1}' groupId='{2}' > <!-- 0-rowId 1-preferenceCategory.idField 2-preferenceGroup.idField --> \
						    <td> </td> <!--empty row--> \
						    <td class='otherFonts'>{3}</td> <!-- 3-preferenceGroup.nameField--> \
		                    <td pid='EMAIL' class='tdEmailSmsLetter'> \
			                    <select class='yesno'> \
				                    <option disabled {4}></option> \
				                    <option value='0' {5}>No</option> \
				                    <option value='1'{6}>Yes</option> \
			                    </select>\
		                    </td>\
		                    <td pid='SMS' class='tdEmailSmsLetter'> \
			                    <select class='yesno'> \
				                    <option disabled {7}></option> \
				                    <option value='0' {8}>No</option> \
				                    <option value='1'{9}>Yes</option> \
			                    </select>\
		                    </td>\
		                    <td pid='LETTER' class='tdEmailSmsLetter'> \
			                    <select class='yesno'> \
				                    <option disabled {10}></option> \
				                    <option value='0' {11}>No</option> \
				                    <option value='1'{12}>Yes</option> \
			                    </select>\
		                    </td>\
					    </tr>";
    /**
     * Dynamically builds the table rows with the required categories and groups data
     * @returns {string}  Html table rows
     */
    appUi.BuildRows = function () {
        var preferenceRowsHtml = "";
        var listOfPreferences = service.FetchAllContactPreferencesForContact();
        if (listOfPreferences == null) {
            preferenceRowsHtml += "<tr> <td class='trCategory header' width='8%'>There are no preference found for the contact.</td></tr>";
        }
        var rowId = -1;
        var emailDropdownValues, smsDropdownValues, letterDropdownValues;
        if (listOfPreferences != null) {
            preferenceRowsHtml += headerTemplate;
            $.each(listOfPreferences.preferenceCategoriesField, function (arrayId, preferenceCategory) {
                if (preferenceCategory != null) {
                    preferenceRowsHtml += stringFormat(categoryTemplate, preferenceCategory.nameField);
                    $.each(preferenceCategory.preferenceGroupsField, function (arrId, preferenceGroup) {
                        preferenceGroup.RowId != null ? rowId = preferenceGroup.RowId : rowId = -1;
                        emailDropdownValues = getMediaDropdownValue(preferenceGroup, "EMAIL");
                        smsDropdownValues = getMediaDropdownValue(preferenceGroup, "SMS");
                        letterDropdownValues = getMediaDropdownValue(preferenceGroup, "LETTER");

                        preferenceRowsHtml += stringFormat(groupTemplate, rowId, preferenceCategory.idField, preferenceGroup.idField, preferenceGroup.nameField,
                            emailDropdownValues[0], emailDropdownValues[1], emailDropdownValues[2],
                            smsDropdownValues[0], smsDropdownValues[1], smsDropdownValues[2],
                            letterDropdownValues[0], letterDropdownValues[1], letterDropdownValues[2]
                            );
                    });
                }
            });
        }
        return preferenceRowsHtml;
    };

    var stringFormat = function () {
        var s = arguments[0];
        for (var i = 0; i < arguments.length - 1; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, arguments[i + 1]);
        }
        return s;
    };

    /**
     * identifies which of the item to be set to seleted in the media dropdowns (ie. blank / No / Yes)
     * @param preferenceGroup - preference group data 
     * @param pid
     * @returns - An array in the order to set the 'selected' attribute of dropdown  options  
     */
    var getMediaDropdownValue = function (preferenceGroup, pid) {
        var optionValue = null;
        var selectedItemArray = ["selected", "", ""]; //blank, No, Yes
        switch (pid) {
            case "EMAIL":
                if (preferenceGroup.Email != null) {
                    if (preferenceGroup.Email === true) {
                        selectedItemArray = ["", "", "selected"];
                    } else if (preferenceGroup.Email === false) {
                        selectedItemArray = ["", "selected", ""];
                    }
                }
                break;
            case "SMS":
                if (preferenceGroup.Sms != null) {
                    if (preferenceGroup.Sms === true) {
                        selectedItemArray = ["", "", "selected"];
                    } else if (preferenceGroup.Sms === false) {
                        selectedItemArray = ["", "selected", ""];
                    }
                }
                break;
            case "LETTER":
                if (preferenceGroup.Letter != null) {
                    if (preferenceGroup.Letter === true) {
                        selectedItemArray = ["", "", "selected"];
                    } else if (preferenceGroup.Letter === false) {
                        selectedItemArray = ["", "selected", ""];
                    }
                }
                break;
            default:
                selectedItemArray = ["selected", "", ""];
                break;
        }

        return selectedItemArray;
    };

    /**
     * This function registers 
     *  1. onchange events the dropdown controls 
     *  2. onclick event for the "Update Preferences" button
     */
    var initializeControlEvents = function () {
        // on change sets the dirty flag on Email/Sms/Letter dropdown controls
        $('select').change(function () {
            var $this = $(this);
            $this.parent().attr('isDirty', true); // cell dirty
            $this.parent().parent().attr('isDirty', true); // row dirty
        });

        // onlcick of the update preferences button.
        $('#updatePreferences').click(function () {
            // Delcaration of the enum
            var mediaSetting = {
                None: 1,
                Email: 2,
                Sms: 4,
                Letter: 8
            };
            var request = {
                "preferences": [],
                "preferenceRequestDetails": {
                    "objectType": App.ObjectType,
                    "objectId": App.ObjectId,
                    "token": App.TokenString
                }
            };

            var rowId = 0;
            // loop through the dirty rows
            $.each($('tr[isDirty]'), function (i, row) {
                var $row = $(row);
                // get the media selected options
                var isEmailDirty = $row.find('td[pid="EMAIL"][isDirty]');
                var isSmsDirty = $row.find('td[pid="SMS"][isDirty]');
                var isLetterDirty = $row.find('td[pid="LETTER"][isDirty]');

                // construct the preference info request
                var preferenceInfo = {
                    "id": parseInt($row.attr('rowId')) <= 0 ? --rowId : parseInt($row.attr('rowId')),
                    "categoryId": $row.attr('categoryId'),
                    "groupId": $row.attr('groupId'),
                    "email": (isEmailDirty && isEmailDirty.length <= 0 ? null : parseInt(isEmailDirty.find('select').val()) == 1 ? true : false),
                    "sms": (isSmsDirty && isSmsDirty.length <= 0 ? null : parseInt(isSmsDirty.find('select').val()) == 1 ? true : false),
                    "letter": (isLetterDirty && isLetterDirty.length <= 0 ? null : parseInt(isLetterDirty.find('select').val()) == 1 ? true : false),
                    "preferencesModified": (isEmailDirty && isEmailDirty.length > 0 ? mediaSetting.Email : 0) |
                                                (isSmsDirty && isSmsDirty.length > 0 ? mediaSetting.Sms : 0) |
                                                (isLetterDirty && isLetterDirty.length > 0 ? mediaSetting.Letter : 0)
                }
                request.preferences.push(preferenceInfo);
            });

            // submit the request
            service.UpdateContactPreferencesForContact(request, function (data) {
                if (data != null && data.validationMessagesField && data.validationMessagesField.length > 0) {
                    alert(data.validationMessagesField[0].messageTextField);
                }
            });

        });
    };
})(App.UI, App.Service);


$(document).ready(
    /**
     * Entry Point - on Page Load
     */
    function init() {
        App.UI.LoadContactPreferences();

    });